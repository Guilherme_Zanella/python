from flask import Blueprint, render_template, request, redirect, url_for, session
from functools import wraps
from mod_usuarios.UsuariosBD import Usuarios

bp_login = Blueprint('login', __name__, url_prefix='/', template_folder='templates')


@bp_login.route("/", methods=['GET', 'POST'])
def login():
    return render_template("formLogin.html")


@bp_login.route("/login", methods=['POST'])
def validaLogin():
    _name = request.form.get('usuario')
    _pass = request.form.get('senha')

    ValidaUserBanco = Usuarios()
    userBanco = ValidaUserBanco.validaUsuario( _name , _pass )

    userNameDB=0
    passwordDB=0
    grupoDB=0
     
    for row in userBanco:
          id = row[0]
          userNameDB=row[1]
          passwordDB=row[2]
          grupoDB=row[3]
          nomeDB=row[4]

    if _name and _pass and request.method == 'POST' and _name == userNameDB and _pass == passwordDB:
        
        session.clear()
        
        session['usuario'] = _name
        session['grupo'] = grupoDB
        session['nome'] = nomeDB
        
        return redirect(url_for('home.home'))
    else:
        
        return redirect(url_for('login.login',falhaLogin=1))


@bp_login.route("/logoff", methods=['GET'])
def validaLogoff():
    
    session.pop('usuario',None)
    
    session.clear()

    
    return redirect(url_for('login.login'))
    

def validaSessao(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'usuario' not in session:
            
            return redirect(url_for('login.login',falhaSessao=1))
        else:
            
            return f(*args, **kwargs)
    
    return decorated_function